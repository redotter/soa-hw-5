#!/usr/bin/env python3

import argparse
import concurrent
import grpc
from redis_queue import RedisQueue

from wiki import WikiGraph

import service_pb2
import service_pb2_grpc


class WikiDistanceServicer(service_pb2_grpc.WikiDistanceServicer):
    def __init__(self, redis):
        self.graph = WikiGraph()
        self.redis_queue = RedisQueue(address=redis)

    def StartJob(self, request, context):
        job_key = self.redis_queue.enqueue(self.graph.find_path, request.start, request.finish)
        return service_pb2.JobKey(key=job_key)

    def GetJobStatus(self, request, context):
        job = self.redis_queue.fetch(request.key)
        return service_pb2.JobStatus(status=job.get_status())

    def GetJobResult(self, request, context):
        job = self.redis_queue.fetch(request.key)
        response = service_pb2.JobResult()
        response.result.extend(job.result)
        return response


class Server:
    def __init__(self, address, redis):
        self.address = address
        self.redis = redis
        self.workers = 10

    def serve(self):
        self.server = grpc.server(
            concurrent.futures.ThreadPoolExecutor(max_workers=self.workers)
        )
        service_pb2_grpc.add_WikiDistanceServicer_to_server(WikiDistanceServicer(self.redis), self.server)
        self.server.add_insecure_port(self.address)
        self.server.start()
        self.server.wait_for_termination()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--address', type=str, default='localhost:9000')
    parser.add_argument('--redis', type=str, default='localhost:6379')
    return parser.parse_args()


def serve():
    args = parse_args()
    server = Server(args.address, args.redis)
    server.serve()


if __name__ == '__main__':
    serve()
