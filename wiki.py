import collections
import logging
import re
import requests
import typing as t

from threading import Thread

logging.getLogger().setLevel(logging.INFO)


class WikiGraph:
    def __init__(self, language='en'):
        self.session = requests.Session()
        self.host = f'https://{language}.wikipedia.org/wiki/'

    def is_valid(self, article: str) -> bool:
        specials = [
            'Category:',
            'File:',
            'Help:',
            'Main_Page',
            'Portal:',
            'Special:',
            'Talk:',
            'Template:',
            'Wikipedia:',
        ]
        for special in specials:
            if article.startswith(special):
                return False
        return True

    def make_article(self, url: str) -> str:
        pattern = f'{self.host}(.*)'
        return re.match(pattern, url).group(1)

    def make_url(self, article: str) -> str:
        return f'{self.host}{article}?redirect=no'
        return self.host + article

    def grab_links_for_url(self, article: str, result: t.Dict[str, t.List]) -> None:
        logging.info(f'Grabbing links for {article}')
        url = self.make_url(article)
        r = self.session.get(url)
        r.raise_for_status()
        pattern = '<a href="/wiki/(\S*)"'
        res = re.findall(pattern, r.content.decode('utf-8'))
        res = set(filter(self.is_valid, res))
        res = sorted(res)
        result[article] = res

    def grab_links(self, articles: t.List[str], target: t.List[str]) -> t.Dict[str, t.List[str]]:
        result = {}
        threads = []
        for article in articles:
            threads.append(Thread(target=self.grab_links_for_url, args=(article, result)))
        mod = 10
        for i, thread in enumerate(threads):
            thread.start()
            if i % mod == 0:
                for j in range(max(0, i - mod + 1), i + 1):
                    threads[j].join()
                    if target in result.get(articles[j], []):
                        return result
        for thread in threads:
            thread.join()
        return result

    def find_path(self, start: str, finish: str) -> t.List[str]:
        start = self.make_article(start)
        finish = self.make_article(finish)

        q = collections.deque()
        used = set()
        d = {}
        parent = {}

        q.append(start)
        used.add(start)
        d[start] = 0

        prev = -1
        layer = [start]
        while q:
            v = q.popleft()
            if v == finish:
                break

            if d[v] > prev:
                layer = sorted(set(layer))
                neighbours = self.grab_links(layer, finish)
                layer = []
                prev = d[v]

            found = False
            for u in neighbours.get(v, []):
                if u not in used:
                    q.append(u)
                    used.add(u)
                    d[u] = d[v] + 1
                    parent[u] = v
                    layer.append(u)
                    if u == finish:
                        found = True
                        break
            if found:
                break

        if finish not in d:
            return path
        path = []
        v = finish
        while v is not None:
            path.append(v)
            v = parent.get(v)
        path.reverse()
        return path
