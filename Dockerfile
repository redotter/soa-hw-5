FROM python:3.7

ENV DEBIAN_FRONTEND noninteractive
ENV PYTHONDONTWRITEBYTECODE 1

COPY . /wiki_distance

RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN python3 -m pip install --upgrade pip
RUN pip install -r /wiki_distance/requirements.txt

ENTRYPOINT ["/wiki_distance/server.py"]
