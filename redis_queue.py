import typing as t

from redis import Redis
from rq import Queue
from rq.job import Job


class RedisQueue:
    def __init__(self, address: str):
        host, port = address.rsplit(':', 1)
        self.redis = Redis(host=host, port=port)
        self.queue = Queue(connection=self.redis)

    def enqueue(self, f: t.Callable, *args) -> str:
        job = self.queue.enqueue(f, *args)
        key = job.key.decode('utf-8')
        key = key.split(':')[-1]
        return key

    def fetch(self, key: str):
        return Job.fetch(key, connection=self.redis)
