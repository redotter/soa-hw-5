#!/usr/bin/env python3

import argparse
import grpc
import time

import service_pb2
import service_pb2_grpc


class Client:
    def __init__(self, stub, start, finish):
        self.stub = stub
        self.start = start
        self.finish = finish

    def start_job(self):
        params = service_pb2.JobParams(start=self.start, finish=self.finish)
        job_key = self.stub.StartJob(params)
        return job_key

    def get_job_status(self, job_key):
        return self.stub.GetJobStatus(job_key)

    def get_job_result(self, job_key):
        return self.stub.GetJobResult(job_key)

    def pretty_print(self, result):
        path = []
        for article in result.result:
            path.append(article)
        print(f'Distance is {len(path)}')
        print('Path:', ' -> '.join(path))

    def run(self):
        job_key = self.start_job()
        status = self.get_job_status(job_key)
        while status.status not in ['finished', 'failed']:
            print('Job status:', status.status)
            time.sleep(1)
            status = self.get_job_status(job_key)
        result = self.get_job_result(job_key)
        self.pretty_print(result)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', type=str, required=True)
    parser.add_argument('--finish', type=str, required=True)
    parser.add_argument('--server', type=str, default='localhost:9000')
    return parser.parse_args()


def main():
    args = parse_args()
    with grpc.insecure_channel(args.server) as channel:
        stub = service_pb2_grpc.WikiDistanceStub(channel)
        client = Client(stub, args.start, args.finish)
        client.run()


if __name__ == '__main__':
    main()
