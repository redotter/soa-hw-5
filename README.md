# Wiki Distance

Run the client:
```bash
./client.py --start https://en.wikipedia.org/wiki/Natalie_Portman --finish https://en.wikipedia.org/wiki/Keira_Knightley --server localhost:9000

Job status: queued
Job status: started
Job status: started
Job status: started
Distance is 3
Path: Natalie_Portman -> Academy_Award_for_Best_Actress -> Keira_Knightley
```

Run the server:
```bash
./server.py --address localhost:9000 --redis localhost:6379
```

Also, redis should be run somewhere (on localhost or remotely):
```bash
nohup redis-server &
nohup rq worker &
```
